## Install ansible in ubuntu 20.04
```bash
sudo apt update

sudo apt install ansible
```

## Set up the inventory
```bash
[defaults]
203.0.113.111 ansible_ssh_user=root
```
## Try pinging the instances
```bash
ansible -m ping all
```
## Run the playbook
```bash
ansible-playbook sftp.yaml
```
